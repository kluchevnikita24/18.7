﻿#include <iostream>



class Stack
{

private:
	int size;
	int* Arr;
	int last_index = 0;
	
	

public:

	Stack(int N)
	{
		size = N;
		Arr = new int[N]; //выделение памяти в конструкторе
	}

	void push(int new_elem)
	{
			
		if (last_index < size)
		{
			
			Arr[last_index++] = new_elem;
		}
	}


	void show()
	{
		for (int i = 0; i < size; i++)
		{

			std::cout << Arr[i] << "\t";

		}
		std::cout << '\n';
	}

	int pop()
	{
		
		if (last_index > 0)
		{
						
			return Arr[last_index--];
		}
	}

	
	~Stack()
	{
		delete[] Arr; // удаление памяти в деструкторе
	}
};


int main()
{
	int N;
	std::cout << "Print N" << '\n';
	std::cin >> N;

	Stack s(N);

	int new_elem;
	std::cin >> new_elem;
	s.push(new_elem);

	s.show();

	s.pop();

	int b = s.pop();
}